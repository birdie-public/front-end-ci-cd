# Deploying a Front-end Application

In this tutorial, we're going to show you how to setup a GitLab CI/CD to deploy a front-end application on Cloudflare Workers.

## Cloudflare Workers

It's a tool that provides an execution of an application, without configuring or maintaining infrastructure. For more information, access their [website](https://workers.cloudflare.com/).

Once you created an account on Cloudflare, you need to choose a subdomain on Workers.

Now you can install [Wrangler CLI](https://developers.cloudflare.com/workers/cli-wrangler):

```sh
npm i @cloudflare/wrangler -g
```

Login with:
```
wrangler login
```

Get your Account ID with:
```
wrangler whoami
```

Copy ``_wrangler.toml`` file to ``wrangler.toml``
```
cp _wrangler.toml wrangler.toml
```

and replace ``ACCOUNT_ID`` with your Account ID.

You can check if it's working by running:
```
yarn generate
wrangler publish --env blue
```

You'll see a success message with the URL of your application.

## GitLab CI/CD

The ``.gitlab-ci.yml`` file is where you configure the CI/CD pipelines.

```yml
# .gitlab-ci.yml

stages:
  - Deploy on Cloudflare

deploy:
  - stage: Deploy on Cloudflare
  - image: node:12-alpine
  script:
    - export USER=$(whoami)
    - yarn
    - yarn deploy $CI_COMMIT_REF_NAME
  only:
    - merge_request
```

This file sets up one job that installs the project dependencies, then executes ``deploy`` script, from the file [``deploy.sh``](deploy.sh) passing the current branch name, or tag, as the argument. The tag ``only`` is where you choose when this job will be triggered, in this case, it will be triggered on every new merge request.

```sh
#!/usr/bin/env sh
NAME=$(echo "$1" | tr '[:upper:]' '[:lower:]' | sed 's/_/-/g')

cp wrangler.toml wrangler.toml.tmp
printf "\n[env.$NAME]\nname=\"$NAME\"\n" >> wrangler.toml
echo $NAME

yarn generate
npx wrangler publish --env $NAME

mv wrangler.toml.tmp wrangler.toml
```

This script gets the argument from the command, transform it to lower case and replace underscores with dashes. This is will be used to create a new enviroment with this name on ``wrangler.toml``. Then executes ``yarn generate`` to generate the website bundle on ``dist`` directory, and publish to Workers sites with wrangler. Finally, ``wrangler.toml`` is reverted to the original state.